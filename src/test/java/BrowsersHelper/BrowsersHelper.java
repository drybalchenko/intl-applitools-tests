package BrowsersHelper;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

public class BrowsersHelper {

    private static String remoteURL = "http://marina67:rDnzuyq1edYevNDxbgNL@hub.browserstack.com/wd/hub";

    public static WebDriver getIE11RemoteWebDriver (String screenSize) {
        DesiredCapabilities capability = new DesiredCapabilities();
        capability.setCapability("browser", "IE");
        capability.setCapability("version", "11");
        capability.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, true);
        capability.setCapability("os", "Windows");
        capability.setCapability("os_version", "10");
        //capability.setCapability("build", "INTL");
        capability.setCapability("acceptSslCerts", "true");
        capability.setCapability("browserstack.debug", "true");
        capability.setCapability("resolution", screenSize);
        capability.setCapability("browserstack.video", "false");
        RemoteWebDriver driver = null;
        try {
            driver = new RemoteWebDriver(
                    new URL(remoteURL),	capability);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        driver.setFileDetector(new LocalFileDetector());
        driver.manage().window().maximize();
        return driver;
    }

    public static WebDriver getChromeRemoteWebDriver (String screenSize) {

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-extensions");

        DesiredCapabilities capability = new DesiredCapabilities();
        capability.setCapability("browser", "chrome");
        capability.setCapability("browser_version", "72.0");
        capability.setCapability("os", "Windows");
        capability.setCapability("os_version", "10");
        capability.setCapability("acceptSslCerts", "true");
        capability.setCapability("browserstack.debug", "true");
        capability.setCapability("resolution", screenSize);
        capability.setCapability("browserstack.video", "false");
        capability.setCapability(ChromeOptions.CAPABILITY, options);
        RemoteWebDriver driver = null;
        try {
            driver = new RemoteWebDriver(
                    new URL(remoteURL),	capability);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        driver.setFileDetector(new LocalFileDetector());
        driver.manage().window().maximize();
        return driver;
    }

    public static WebDriver getFirefoxRemoteWebDriver (String screenSize) {

        //FirefoxOptions options = new FirefoxOptions();
        //options.addArguments("--disable-extensions");

        DesiredCapabilities capability = new DesiredCapabilities();
        capability.setCapability("browser", "Firefox");
        capability.setCapability("browser_version", "58.0"); //60 61.0.2    58
        capability.setCapability("os", "Windows");
        capability.setCapability("os_version", "10");
        capability.setCapability("acceptSslCerts", "true");
        capability.setCapability("browserstack.debug", "true");
        capability.setCapability("resolution", screenSize);
        capability.setCapability("browserstack.video", "false");
        //capability.setCapability(FirefoxOptions.FIREFOX_OPTIONS, options);
        RemoteWebDriver driver = null;
        try {
            driver = new RemoteWebDriver(
                    new URL(remoteURL),	capability);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        driver.setFileDetector(new LocalFileDetector());
        driver.manage().window().maximize();
        return driver;
    }

    public static WebDriver getEdgeRemoteWebDriver (String screenSize) {
        DesiredCapabilities capability = new DesiredCapabilities();
        capability.setCapability("os", "Windows");
        capability.setCapability("os_version", "10");
        capability.setCapability("browser", "Edge");
        capability.setCapability("browser_version", "18");
        capability.setCapability("resolution", screenSize);
        capability.setCapability("browserstack.local", "false");
        capability.setCapability("browserstack.selenium_version", "3.5.2");

        RemoteWebDriver driver = null;
        try {
            driver = new RemoteWebDriver(
                    new URL(remoteURL),	capability);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        driver.setFileDetector(new LocalFileDetector());
        driver.manage().window().maximize();
        return driver;
    }
}
