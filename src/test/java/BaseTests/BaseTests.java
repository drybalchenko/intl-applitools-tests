package BaseTests;

import Applitools.Applitool;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import com.codeborne.selenide.Selenide;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class BaseTests {

    public static Applitool at;

    @BeforeClass
    public static void setup(){
        initializePages();
    }

    private static void initializePages() {
        at = Selenide.page (Applitool.class);
    }
}
