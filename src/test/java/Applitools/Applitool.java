package Applitools;

import BrowsersHelper.BrowsersHelper;
import org.junit.*;
import org.openqa.selenium.WebDriver;
import com.applitools.eyes.BatchInfo;
import com.applitools.eyes.selenium.Eyes;

public class Applitool{

    public static BatchInfo batch;
    public Eyes eyes;
    public WebDriver browser;

    @BeforeClass
    public static void batch(){
        batch = new BatchInfo("INTL tests");
    }

    @Before
    public void setup2(){
        eyes = new Eyes();
        eyes.setApiKey("nuaxivuuwircL0H3y3FtOM0CGG0SN3WHQGdGhEDmdlQ110");
        eyes.setBatch(batch);
    }

    public void intlMain(WebDriver driver, String testName) throws InterruptedException {
        browser = driver;
        eyes.open(browser, "INTL", testName);

        browser.get("https://mercurydev-mercurydev.cs95.force.com/s/marketintel?language=en_US");
        Thread.sleep(20000);
        eyes.checkWindow("'Home' Page");

        browser.get("https://mercurydev-mercurydev.cs95.force.com/s/free-trial?language=en_US");
        Thread.sleep(20000);
        eyes.checkWindow("'Free Trial' page");

        browser.get("https://mercurydev-mercurydev.cs95.force.com/s/author?language=en_US&authorId=4306");
        Thread.sleep(20000);
        eyes.checkWindow("'Author' page");

        browser.get("https://mercurydev-mercurydev.cs95.force.com/s/team?language=en_US&marketId=336");
        Thread.sleep(20000);
        eyes.checkWindow("'Base Metals Authors' page");

        browser.get("https://mercurydev-mercurydev.cs95.force.com/s/article?language=en_US&articleId=89899174-4e24-4b7b-8de9-4558688828c2");
        Thread.sleep(20000);
        eyes.checkWindow("Page with Publication");

        //https://mfa-beta.intlfcstone.com/secureauth47/
/*
        browser.get("https://mercurydev-mercurydev.cs95.force.com/s/marketintel?language=en_US");
        eyes.checkWindow("'Home' Page 2");

        browser.get("https://mercurydev-mercurydev.cs95.force.com/s/category?language=en_US&marketId=246");
        eyes.checkWindow("Grains & Oilseeds");

        browser.get("https://mercurydev-mercurydev.cs95.force.com/s/subcategory?language=en_US&commodityId=271");
        eyes.checkWindow("Rough Rice");

        browser.get("https://mercurydev-mercurydev.cs95.force.com/s/subcategory?language=en_US&commodityId=266");
        eyes.checkWindow("Oats");

        browser.get("https://mercurydev-mercurydev.cs95.force.com/s/subcategory?language=en_US&commodityId=276");
        eyes.checkWindow("General");

        browser.get("https://mercurydev-mercurydev.cs95.force.com/s/subcategory?language=en_US&commodityId=261");
        eyes.checkWindow("Corn");

        browser.get("https://mercurydev-mercurydev.cs95.force.com/s/subcategory?language=en_US&commodityId=251");
        eyes.checkWindow("Soybeans");

        browser.get("https://mercurydev-mercurydev.cs95.force.com/s/subcategory?language=en_US&commodityId=256");
        eyes.checkWindow("Wheat");

        browser.get("https://mercurydev-mercurydev.cs95.force.com/s/team?language=en_US&marketId=246");
        eyes.checkWindow("Oats");
*/
        browser.close();
        eyes.close();
    }

    @Test
    public void test01() throws InterruptedException {
        intlMain(BrowsersHelper.getFirefoxRemoteWebDriver("1024x768"), "Firefox- 1024x768");
    }

    @Test
    public void test02() throws InterruptedException {
        intlMain(BrowsersHelper.getFirefoxRemoteWebDriver("1280x800"), "Firefox- 1280x800");
    }

    @Test
    public void test03() throws InterruptedException {
        intlMain(BrowsersHelper.getFirefoxRemoteWebDriver("1280x1024"), "Firefox- 1280x1024");
    }

    @Test
    public void test04() throws InterruptedException {
        intlMain(BrowsersHelper.getFirefoxRemoteWebDriver("1440x900"), "Firefox- 1440x900");
    }

    @Test
    public void test05() throws InterruptedException {
        intlMain(BrowsersHelper.getFirefoxRemoteWebDriver("1920x1200"), "Firefox- 1920x1200");
    }

    @Test
    public void test06() throws InterruptedException {
        intlMain(BrowsersHelper.getFirefoxRemoteWebDriver("1920x1080"), "Firefox- 1920x1080");
    }

    @Test
    public void test07() throws InterruptedException {
        intlMain(BrowsersHelper.getChromeRemoteWebDriver("1024x768"), "Chrome- 1024x768");
    }

    @Test
    public void test08() throws InterruptedException {
        intlMain(BrowsersHelper.getChromeRemoteWebDriver("1280x800"), "Chrome- 1280x800");
    }

    @Test
    public void test09() throws InterruptedException {
        intlMain(BrowsersHelper.getChromeRemoteWebDriver("1280x1024"), "Chrome- 1280x1024");
    }

    @Test
    public void test10() throws InterruptedException {
        intlMain(BrowsersHelper.getChromeRemoteWebDriver("1440x900"), "Chrome- 1440x900");
    }

    @Test
    public void test11() throws InterruptedException {
        intlMain(BrowsersHelper.getChromeRemoteWebDriver("1920x1200"), "Chrome- 1920x1200");
    }

    @Test
    public void test12() throws InterruptedException {
        intlMain(BrowsersHelper.getChromeRemoteWebDriver("1920x1080"), "Chrome- 1920x1080");
    }

    @Test
    public void test13() throws InterruptedException {
        intlMain(BrowsersHelper.getIE11RemoteWebDriver("1024x768"), "IE11- 1024x768");
    }

    @Test
    public void test14() throws InterruptedException {
        intlMain(BrowsersHelper.getIE11RemoteWebDriver("1280x800"), "IE11- 1280x800");
    }

    @Test
    public void test15() throws InterruptedException {
        intlMain(BrowsersHelper.getIE11RemoteWebDriver("1280x1024"), "IE11- 1280x1024");
    }

    @Test
    public void test16() throws InterruptedException {
        intlMain(BrowsersHelper.getIE11RemoteWebDriver("1440x900"), "IE11- 1440x900");
    }

    @Test
    public void test17() throws InterruptedException {
        intlMain(BrowsersHelper.getIE11RemoteWebDriver("1920x1200"), "IE11- 1920x1200");
    }

    @Test
    public void test18() throws InterruptedException {
        intlMain(BrowsersHelper.getIE11RemoteWebDriver("1920x1080"), "IE11- 1920x1080");
    }

    @Test
    public void test19() throws InterruptedException {
        intlMain(BrowsersHelper.getEdgeRemoteWebDriver("1024x768"), "Edge- 1024x768");
    }

    @Test
    public void test20() throws InterruptedException {
        intlMain(BrowsersHelper.getEdgeRemoteWebDriver("1280x800"), "Edge- 1280x800");
    }

    @Test
    public void test21() throws InterruptedException {
        intlMain(BrowsersHelper.getEdgeRemoteWebDriver("1280x1024"), "Edge- 1280x1024");
    }

    @Test
    public void test22() throws InterruptedException {
        intlMain(BrowsersHelper.getEdgeRemoteWebDriver("1440x900"), "Edge- 1440x900");
    }

    @Test
    public void test23() throws InterruptedException {
        intlMain(BrowsersHelper.getEdgeRemoteWebDriver("1920x1200"), "Edge- 1920x1200");
    }

    @Test
    public void test24() throws InterruptedException {
        intlMain(BrowsersHelper.getEdgeRemoteWebDriver("1920x1080"), "Edge- 1920x1080");
    }
}